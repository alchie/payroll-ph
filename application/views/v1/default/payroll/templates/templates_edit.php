<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('payroll/payroll_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Edit Template</h3>
        </div>
        <form method="post">
        <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>


      <div class="row">
          <div class="col-md-10">
              <div class="form-group">
                <label>Template Name</label>
                <input name="name" type="text" class="form-control" value="<?php echo $template->name; ?>">
              </div>
          </div>
          <div class="col-md-2">
                   <div class="form-group">
                    <label>Pages</label>
                    <input name="pages" type="text" class="form-control text-right" value="<?php echo $template->pages; ?>">
                  </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
                  <div class="form-group">
                    <label>Checked By</label>
                    <input id="checked_by_name_id" name="checked_by" type="hidden" value="<?php echo $template->checked_by; ?>">
                    <input name="" class="form-control autocomplete-name_select autocomplete-name_select-name-input-<?php echo time(); ?>" data-source="<?php echo site_url("payroll_templates/ajax/search_name"); ?>" data-name_id="checked_by_name_id" type="text" style="display: none;">
                    <div class="form-control autocomplete-name_select-name-display-<?php echo time(); ?>"><a class="badge" id="changeName" href="#changeName" data-id="<?php echo $template->checked_by; ?>" data-name_id="checked_by_name_id" data-timestamp="<?php echo time(); ?>"><?php echo ($template->checked_by_name) ? $template->checked_by_name : 'not assigned'; ?></a></div>
                  </div>
          </div>
          <div class="col-md-6">
                  <div class="form-group">
                    <label>Approved by</label>
                    <input id="approved_by_name_id" name="approved_by" type="hidden" value="<?php echo $template->approved_by; ?>">
                    <input name="" class="form-control autocomplete-name_select autocomplete-name_select-name-input-<?php echo time(); ?>-2" data-source="<?php echo site_url("payroll_templates/ajax/search_name"); ?>" data-name_id="approved_by_name_id" type="text" style="display: none;">
                    <div class="form-control autocomplete-name_select-name-display-<?php echo time(); ?>-2"><a class="badge" id="changeName" href="#changeName" data-id="<?php echo $template->approved_by; ?>" data-name_id="approved_by_name_id" data-timestamp="<?php echo time(); ?>-2"><?php echo ($template->approved_by_name) ? $template->approved_by_name : 'not assigned'; ?></a></div>
                  </div>
          </div>
        </div>

      <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                <label>Print Format</label>
                <select name="print_format" class="form-control">
                    <option value="">Default</option>
                    <option value="clergy_allowance" <?php echo ($template->print_format=='clergy_allowance') ? 'SELECTED' : ''; ?>>Clergy Allowance</option>
                </select>
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                <label>Group By</label>
                <select name="group_by" class="form-control">
                    <option value="group" <?php echo ($template->group_by=='group') ? 'SELECTED' : ''; ?>>Group (Default)</option>
                    <option value="area" <?php echo ($template->group_by=='area') ? 'SELECTED' : ''; ?>>Area</option>
                    <option value="position" <?php echo ($template->group_by=='position') ? 'SELECTED' : ''; ?>>Position</option>
                    <option value="status" <?php echo ($template->group_by=='status') ? 'SELECTED' : ''; ?>>Status</option>
                </select>
              </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
                  <div class="form-group">
                    <label>Use Payroll</label>
                    <input id="use_payroll" name="payroll_id" type="hidden" value="<?php echo $template->payroll_id; ?>">
                    <input name="" class="form-control autocomplete-name_select autocomplete-name_select-name-input-<?php echo time(); ?>-3" data-source="<?php echo site_url("payroll_templates/ajax/search_payroll"); ?>" data-name_id="use_payroll" type="text" style="display: none;">
                    <div class="form-control autocomplete-name_select-name-display-<?php echo time(); ?>-3"><a class="badge" id="changeName" href="#changePayroll" data-id="<?php echo $template->payroll_id; ?>" data-name_id="use_payroll" data-timestamp="<?php echo time(); ?>-3"><?php echo ($template->payroll_id) ? $template->payroll_name : 'not assigned'; ?></a></div>
                  </div>
          </div>
          <div class="col-md-6">

          </div>
        </div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url($current_uri); ?>" class="btn btn-warning">Back</a>
        </div>
        </form>
      </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>