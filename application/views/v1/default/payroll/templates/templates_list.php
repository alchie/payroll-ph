<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('payroll/payroll_navbar'); ?>

<div class="container">
    <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
<?php if( hasAccess('payroll', 'templates', 'add') ) { ?>
  <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Template" data-url="<?php echo site_url("payroll_templates/add/ajax") . "?next=" . uri_string(); ?>" style="margin-right: 5px">Create Template</button>
<?php } ?>
                  <h3 class="panel-title bold"><?php echo $current_page; ?>

  <a href="<?php echo site_url(uri_string()); ?>?filter=trash"><span class="glyphicon glyphicon-trash"></span></a>

                  </h3>
                </div>
                <div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>

<?php if( $templates ) { ?>

          <table class="table table-default">
            <thead>
              <tr>
                <th>Template Name</th>
                <th>Payroll Count</th>
                <th>Copy Data From Payroll</th>
                <?php if( hasAccess('payroll', 'templates', 'edit') ) { ?>
                  <th width="260px" class="text-right">Action</th>
                <?php } ?>
              </tr>
            </thead>
            <tbody>
            <?php foreach($templates as $template) {  ?>
              <tr id="template-<?php echo $template->id; ?>">
                <td><?php echo $template->name; ?></td>
                <td><a class="body_wrapper" href="<?php echo site_url("payroll/index/0/0/{$template->id}"); ?>"><?php echo $template->payroll_count; ?></a></td>
                <td><?php echo $template->template_name; ?></td>
              <?php if( hasAccess('payroll', 'templates', 'edit') ) { ?>
                <td class="text-right">
<?php if( $template->active ) { ?>
<div class="btn-group">
                  <button type="button" class="btn btn-info btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Configure Template" data-url="<?php echo site_url("payroll_templates/config/{$template->id}/ajax") . "?next=" . uri_string(); ?>" data-hide_footer="1">Config</button>
  <button type="button" class="btn btn-info dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">

  <li><a href="#ajaxModal" data-target="#ajaxModal" data-title="Template Details" data-toggle="modal" class="ajax-modal" data-url="<?php echo site_url("payroll_templates/edit/{$template->id}/ajax") . "?next=" . uri_string(); ?>">Template Details</a></li>

<?php if( !$template->payroll_id ) { ?>
  <li><a href="#ajaxModal" data-target="#ajaxModal"  data-title="Employee Groups" data-toggle="modal" class="ajax-modal" data-url="<?php echo site_url("payroll_templates/groups/{$template->id}/ajax") . "?next=" . uri_string(); ?>">Employee Groups</a></li>

  <li><a href="#ajaxModal" data-target="#ajaxModal" data-title="Earnings" data-toggle="modal" class="ajax-modal" data-url="<?php echo site_url("payroll_templates/earnings/{$template->id}/ajax") . "?next=" . uri_string(); ?>">Earnings</a></li>

  <li><a href="#ajaxModal" data-target="#ajaxModal" data-title="Benefits" data-toggle="modal" class="ajax-modal" data-url="<?php echo site_url("payroll_templates/benefits/{$template->id}/ajax") . "?next=" . uri_string(); ?>">Benefits</a></li>

  <li><a href="#ajaxModal" data-target="#ajaxModal" data-title="Deductions" data-toggle="modal"  class="ajax-modal" data-url="<?php echo site_url("payroll_templates/deductions/{$template->id}/ajax") . "?next=" . uri_string(); ?>">Deductions</a></li>
<?php } ?>

  <li><a href="#ajaxModal" data-target="#ajaxModal" data-title="Print Columns" data-toggle="modal" class="ajax-modal" data-url="<?php echo site_url("payroll_templates/print_columns/{$template->id}/ajax") . "?next=" . uri_string(); ?>">Print Columns</a></li>
  </ul>
</div>

                <a class="btn btn-danger btn-xs confirm_remove" href="<?php echo site_url("payroll_templates/deactivate/{$template->id}"). "?next=" . uri_string(); ?>" data-target="#template-<?php echo $template->id; ?>">Deactivate</a>

                <a class="btn btn-warning btn-xs" href="<?php echo site_url("payroll_templates/select_preview/{$template->id}"); ?>">Preview</a>

<?php } else { ?>

  <a class="btn btn-success btn-xs confirm" href="<?php echo site_url("payroll_templates/restore/{$template->id}") . "?next=" . uri_string(); ?>">Restore</a>

  <a class="btn btn-danger btn-xs confirm_remove" href="<?php echo site_url("payroll_templates/delete/{$template->id}"). "?next=" . uri_string(); ?>">Delete Permanently</a>

<?php } ?>
                </td>
              <?php } ?>
              </tr>
            <?php } ?>

            </tbody>
          </table>

          <?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

  <div class="text-center">No Template Found!</div>

<?php } ?>

<?php if( ! $inner_page ): ?>

              </div>
              </div>
            </div>
    </div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>