<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$payslip_templates = unserialize(PAYROLL_PAYSLIP_TEMPLATES);
?>
<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('payroll/payroll_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">

<div class="btn-group pull-right" role="group" aria-label="..." style="margin-bottom: 5px;">
  
  <button class="btn btn-default btn-xs sortable-asc1 accordion-sort-asc" data-sortable="sortable-employees" type="button"><span class="glyphicon glyphicon-sort-by-alphabet"></span></button> 

  <button class="btn btn-default btn-xs sortable-desc1 accordion-sort-desc" data-sortable="sortable-employees" type="button"><span class="glyphicon glyphicon-sort-by-alphabet-alt"></span></button> 

  <a class="btn btn-default btn-xs" href="<?php echo site_url("payroll/add_employee/{$payroll_id}/{$group_id}/{$output}") . "?next=" . $this->input->get("next"); ?>"><span class="glyphicon glyphicon-plus"></span> <span class="glyphicon glyphicon-user"></span></a> 

</div>

          <h3 class="panel-title">
<?php if($this->input->get('action')!='sort') { ?>
  Configure Payroll Employees
<?php } else { ?>
  Sort Payroll Employees
<?php } ?>
          </h3>
        </div>
<form method="post">
        <div class="panel-body">
  <?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<?php if( isset($output) && ($output=='ajax') ) { ?>

<div class="col-md-5 pull-right">
  <input type="text" class="form-control input-sm filter-list-name" data-list="accordion" placeholder="Filter Name...">
</div>

<div class="btn-group" role="group" aria-label="..." style="margin-bottom: 5px;">
  <button class="btn btn-default btn-xs sortable-asc1 accordion-sort-asc" data-sortable="sortable-employees" type="button"><span class="glyphicon glyphicon-sort-by-alphabet"></span></button> 
  <button class="btn btn-default btn-xs sortable-desc1 accordion-sort-desc" data-sortable="sortable-employees" type="button"><span class="glyphicon glyphicon-sort-by-alphabet-alt"></span></button> 
<?php if($this->input->get('action')!='sort') { ?>
  <a class="btn btn-default btn-xs ajax-modal-inner" href="<?php echo site_url("payroll/add_employee/{$payroll_id}/{$group_id}/{$output}") . "?next=" . $this->input->get("next"); ?>"><span class="glyphicon glyphicon-plus"></span> <span class="glyphicon glyphicon-user"></span></a> 
<?php } ?>
</div>
<?php } ?>

<div class="panel-group sortable sortable-employees" id="accordion" role="tablist" aria-multiselectable="true">
<?php foreach($employees as $employee) { ?>
   <div class="panel panel-<?php echo ($employee->active==1) ? (($employee->manual==1)?'warning':'default') : 'danger'; ?>">
    <div class="panel-heading" role="tab" id="heading<?php echo $employee->pe_id; ?>">
      <h4 class="panel-title">
        <label><input type="checkbox" name="selected[]" value="<?php echo $employee->pe_id; ?>" <?php echo ($employee->active==1) ? "CHECKED" : ""; ?>></label>
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $employee->pe_id; ?>" aria-expanded="true" aria-controls="collapseOne">
          <input type="hidden" name="pe_id[]" value="<?php echo $employee->pe_id; ?>">
          <span class="glyphicon glyphicon-sort pull-right" style="margin-left: 10px;"></span>
           <?php echo $employee->lastname; ?>, <?php echo $employee->firstname; ?> <?php echo substr($employee->middlename,0,1)."."; ?>
        </a>
      </h4>
    </div>
    
<?php if($this->input->get('action')!='sort') { ?>
<?php if($employee->active==1 && $employee->manual==0) { ?>
    <div id="collapse<?php echo $employee->pe_id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $employee->pe_id; ?>">
      <div class="panel-body">


<div class="row" style="margin-top:10px;">
  <div class="col-md-6 col-sm-6 col-xs-6">
    <select class="form-control input-sm" name="group[<?php echo $employee->pe_id; ?>]" data-style="btn-default btn-sm">
      <option value="0">- - No Group - -</option>
<?php foreach( $groups as $group) { ?>
          <option value="<?php echo $group->id; ?>" <?php echo ($employee->group_id2==$group->id) ? 'SELECTED' : ''; ?>><?php echo $group->name; ?></option>
<?php } ?>
      </select>
  </div>
  <div class="col-md-6 col-sm-6 col-xs-6">
    <select class="form-control input-sm" name="position[<?php echo $employee->pe_id; ?>]" data-style="btn-default btn-sm">
      <option value="0">- - No Position - -</option>
<?php foreach( $positions as $position) { ?>
          <option value="<?php echo $position->id; ?>" <?php echo ($employee->position_id==$position->id) ? 'SELECTED' : ''; ?>><?php echo $position->name; ?></option>
<?php } ?>
      </select>
  </div>
</div>

<div class="row" style="margin-top:10px;">
  <div class="col-md-6 col-sm-6 col-xs-6">
    <select class="form-control input-sm" name="area[<?php echo $employee->pe_id; ?>]" data-style="btn-default btn-sm">
      <option value="0">- - No Area - -</option>
<?php foreach( $areas as $area) { ?>
          <option value="<?php echo $area->id; ?>" <?php echo ($employee->area_id==$area->id) ? 'SELECTED' : ''; ?>><?php echo $area->name; ?></option>
<?php } ?>
      </select>
  </div>
  <div class="col-md-6 col-sm-6 col-xs-6">
    <select class="form-control input-sm" name="status[<?php echo $employee->pe_id; ?>]" data-style="btn-default btn-sm">
      <option value="0">- - No Assigned Status - -</option>
<?php foreach( $employment_status as $status) { ?>
          <option value="<?php echo $status->id; ?>" <?php echo ($employee->status==$status->id) ? 'SELECTED' : ''; ?>><?php echo $status->name; ?></option>
<?php } ?>
      </select>
  </div>
</div>


<div class="row" style="margin-top:10px;">
  <div class="col-md-6 col-sm-6 col-xs-6">
    <select class="form-control input-sm" name="payslip_template[<?php echo $employee->pe_id; ?>]" data-style="btn-default btn-sm">
      <option value="">- - No Payslip - -</option>
<?php foreach( $payslip_templates as $k=>$v) { ?>
          <option value="<?php echo $k; ?>" <?php echo ($employee->template==$k) ? 'SELECTED' : ''; ?>><?php echo $v; ?></option>
<?php } ?>
      </select>
  </div>
  <div class="col-md-6 col-sm-6 col-xs-6">
      <?php if( $print_groups ) { ?>
            <select class="form-control input-sm" name="print_group[<?php echo $employee->pe_id; ?>]" data-style="btn-default btn-sm">
              <option value="none">No Print Group</option>
              <?php foreach($print_groups as $grp) { ?>
                <option value="<?php echo $grp->id; ?>" <?php echo ($employee->print_group==$grp->id) ? "SELECTED" : ""; ?>><?php echo $grp->name; ?></option>
              <?php } ?>
            </select>
    <?php } ?>
  </div>
</div>

      </div>
    </div>
<?php } ?>
<?php } ?>

  </div>
<?php } ?>
</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>
        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url($current_uri); ?>" class="btn btn-warning">Back</a>
        </div>
        </form>
      </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>