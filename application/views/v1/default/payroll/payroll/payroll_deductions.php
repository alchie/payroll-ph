<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('payroll/payroll_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Configure Payroll Template</h3>
        </div>

        <div class="panel-body">
  <?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="col-md-5 pull-right">
  <input type="text" class="form-control input-sm filter-list-name2" data-list="list-group" placeholder="Filter Name...">
</div>

<div class="btn-group" role="group" aria-label="..." style="margin-bottom: 5px;">
  <button class="btn btn-default btn-xs sortable-asc" data-sortable="sortable" type="button"><span class="glyphicon glyphicon-sort-by-alphabet"></span></button> 
  <button class="btn btn-default btn-xs sortable-desc" data-sortable="sortable" type="button"><span class="glyphicon glyphicon-sort-by-alphabet-alt"></span></button> 
</div>

<ul class="list-group sortable">
  <?php foreach($deductions as $deduction) { ?>
  <li class="list-group-item">
  <input type="hidden" name="deduction[]" value="<?php echo $deduction->id; ?>">
  <span class="glyphicon glyphicon-sort pull-right"></span>
  <?php if($deduction->id==$deduction->selected) { ?>
  <a class="pull-right" href="<?php echo site_url("payroll_deductions/reset/{$payroll_id}/{$deduction->id}") . "?next=" . $this->input->get('next'); ?>" style="margin-right:5px;"><span class="glyphicon glyphicon-refresh"></span></a>
  <?php } ?>
    <h4 class="list-group-item-heading"><label><input type="checkbox" name="selected[]" value="<?php echo $deduction->id; ?>" <?php echo ($deduction->id==$deduction->selected) ? "CHECKED" : ""; ?>> <?php echo $deduction->name; ?></label></h4>
    <p class="list-group-item-text"><?php echo $deduction->notes; ?></p>
  </li>
  <?php } ?>
</ul>

<?php if( isset($output) && ($output!='ajax') ) : ?>
        </div>

      </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>