<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$query_string = '?';
if($this->input->get()) {
  foreach($this->input->get() as $key=>$value) {
    $query_string .= $key . "=" . $value . "&";
  }
}
?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('payroll/payroll/payroll_view_navbar'); ?>

<div class="container">
    <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">

                <a class="pull-right close" href="<?php echo site_url("payroll_deductions/item_schedule/{$payroll->id}/{$deduction_data->id}/print") . $query_string; ?>" target="_blank"><span class="glyphicon glyphicon-print"></span></a>
<?php if(!$this->input->get('equalizer')) { ?>
<a class="pull-right close" href="<?php echo site_url("payroll_deductions/item_schedule/{$payroll->id}/{$deduction_data->id}") . $query_string; ?>equalizer=1"  style="margin-right: 5px"><span class="glyphicon glyphicon-equalizer"></span></a>
<?php } ?>

                  <h3 class="panel-title"><strong><?php echo $deduction_data->name; ?> - <?php echo $deduction_data->notes; ?></strong> 
                  <?php if( !$this->input->get('remove_grouping')) { ?>
                  <small class="badge">Grouped by Name <a href="<?php echo site_url(uri_string()) . $query_string . "remove_grouping=1"; ?>" class="glyphicon glyphicon-remove" title="Remove Grouping"></a></small>
                  <?php } ?>
                  </h3>

                </div>
                <div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>

<?php if( $item_data ) {  ?>
<form method="post">
          <table class="table table-default table-hover" id="Payroll-Group">
            <thead>
              <tr class="warning">
                <th>Employee Name</th>
                <th width="13%" class="text-right">Total Payables</th>
                <th width="13%" class="text-right">Amount Paid</th>
                <th width="13%" class="text-right">Balance</th>
                <th width="13%" class="text-right">Current Deductions</th>
                <th width="13%" class="text-right">Total Amount Paid</th>
                <th width="13%" class="text-right">New Balance</th>
<?php if( $this->input->get('remove_grouping')) { ?>
                <th width="1%" class="text-right">
                  <button class="glyphicon glyphicon-remove btn btn-danger btn-xs confirm" type="submit"></button>
                </th>
<?php } ?>
              </tr>
            </thead>
            <tbody>
            
<?php 
$total_max_amount = 0;
$total_paid = 0;
$total_balance = 0;
$total_payment = 0;
$total_amount_payment = 0;
$total_new_balance = 0;

foreach($item_data as $item) {

$total_max_amount += $item->max_amount;
$total_paid += $item->amount_paid;
$item_balance = ($item->max_amount > 0) ? ($item->max_amount - $item->amount_paid) : 0;
$total_balance += $item_balance;
$total_payment += $item->amount;
$total_amount_payment += $item->amount_paid+$item->amount;
$total_new_balance += ($item->max_amount-($item->amount_paid+$item->amount));
              ?>
              <tr>
                <td><?php echo $item->lastname; ?>, <?php echo $item->firstname; ?> <?php echo substr($item->middlename,0,1)."."; ?>
<?php if(!$payroll->lock) { ?>
                <a href="<?php echo site_url("employees_deductions/view/{$item->name_id}") . "?next=" . uri_string(); ?>" class="body_wrapper"><span class="glyphicon glyphicon-cog"></span></a>
<?php } ?>
                </td>
                <td class="text-right"><?php echo number_format($item->max_amount,2); ?></td>
                <td class="text-right">
<?php if(!$payroll->lock) { ?>
<a class="ajax-modal" href="#ajaxModal" data-toggle="modal" data-target="#ajaxModal" data-title="<?php echo $item->lastname; ?>, <?php echo $item->firstname; ?> <?php echo substr($item->middlename,0,1)."."; ?> - Amount Paid" data-url="<?php echo site_url("employees_deductions/entries/{$item->entry_id}/ajax") . "?no_action=1&exempt={$item->id}&next=" . uri_string(); ?>" data-hide_footer="1">
<?php } ?>
                <?php echo number_format($item->amount_paid,2); ?>
<?php if(!$payroll->lock) { ?>
</a>
<?php } ?>
                </td>
                <td class="text-right"><?php echo number_format($item_balance,2); ?></td>
                <td class="text-right bold">
<?php if(!$payroll->lock) { ?>
<a class="ajax-modal" href="#ajaxModal" data-toggle="modal" data-target="#ajaxModal" data-title="<?php echo $item->lastname; ?>, <?php echo $item->firstname; ?> <?php echo substr($item->middlename,0,1)."."; ?> - Current Deduction" data-url="<?php echo site_url("payroll_deductions/edit/{$item->id}/ajax") . "?edit_only=1&next=" . uri_string(); ?>">
<?php } ?>
                <?php echo number_format($item->amount,2); ?>
<?php if(!$payroll->lock) { ?>
</a>
<?php } ?>
                </td>
                <td class="text-right"><?php echo number_format(($item->amount_paid+$item->amount),2); ?></td>
                <td class="text-right">
<?php if(!$payroll->lock) { ?>
<a class="ajax-modal" href="#ajaxModal" data-toggle="modal" data-target="#ajaxModal" data-title="<?php echo $item->lastname; ?>, <?php echo $item->firstname; ?> <?php echo substr($item->middlename,0,1)."."; ?> - New Balance" data-url="<?php echo site_url("employees_deductions/entries/{$item->entry_id}/ajax") . "?no_action=1&next=" . uri_string(); ?>" data-hide_footer="1">
<?php } ?>
                <?php echo number_format(($item->max_amount-($item->amount_paid+$item->amount)),2); ?>
<?php if(!$payroll->lock) { ?>
</a>
<?php } ?>
                </td>
<?php if( $this->input->get('remove_grouping')) { ?>
                <td class="text-right"><input type="checkbox" name="remove_item[]" value="<?php echo $item->id; ?>"></td>
<?php } ?>
              </tr>
<?php } ?>

              <tr class="success">
                <td><strong>Total</strong></td>
                <td class="text-right"><strong><?php echo number_format($total_max_amount,2); ?></strong></td>
                <td class="text-right"><strong><?php echo number_format($total_paid,2); ?></strong></td>
                <td class="text-right"><strong><?php echo number_format($total_balance,2); ?></strong></td>
                <td class="text-right"><strong><?php echo number_format($total_payment,2); ?></strong></td>
                <td class="text-right"><strong><?php echo number_format($total_amount_payment,2); ?></strong></td>
                <td class="text-right"><strong><?php echo number_format($total_new_balance,2); ?></strong></td>
<?php if( $this->input->get('remove_grouping')) { ?>
                <td class="text-right"><button class="glyphicon glyphicon-remove btn btn-danger btn-xs confirm" type="submit"></button></td>
<?php } ?>
              </tr>
            </tbody>
          </table>


<?php } else { ?>

    <div class="text-center">No Item Found!</div>

<?php }  ?>

<?php if( ! $inner_page ): ?>

              </div>
              </div>
            </div>
    </div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>