<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
function isColumn($ths, $column_id,$print_columns) {
    if( $ths->input->get('columns') ) {
        if( in_array($column_id, $ths->input->get('columns')) ) {
            return true;
        }
    } else {
      if( $print_columns ) foreach($print_columns as $col) {
          if( ($col->column_id==$column_id) ) {
              return true;
          }
      }
    }
    return false;
}  
?>
<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>chesteralan</Author>
  <LastAuthor>chesteralan</LastAuthor>
  <LastPrinted><?php echo date("Y-m-d"); ?>T<?php echo date("H:i:s"); ?>Z</LastPrinted>
  <Created><?php echo date("Y-m-d"); ?>T<?php echo date("H:i:s"); ?>Z</Created>
  <Version>12.00</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>11760</WindowHeight>
  <WindowWidth>20730</WindowWidth>
  <WindowTopX>360</WindowTopX>
  <WindowTopY>345</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>

  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s16" ss:Name="Comma">
   <NumberFormat ss:Format="_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"/>
  </Style>
  <Style ss:ID="m79168628">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s62">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s63">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s64">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#CCCCCC"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s65">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#CCCCCC"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s651" ss:Parent="s16">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#CCCCCC"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s66" ss:Parent="s16">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#CCCCCC"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s67">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#CCCCCC"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s68">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#CCCCCC"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s69">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#CCCCCC"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s70">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#CCCCCC"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s71">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#CCCCCC"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s711" ss:Parent="s16">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#CCCCCC"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s72" ss:Parent="s16">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#CCCCCC"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s73" ss:Parent="s16">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#CCCCCC"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s74">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#CCCCCC"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s75">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s76">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s77">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s78">
   <Alignment ss:Horizontal="Right" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="Standard"/>
  </Style>
  <Style ss:ID="s80">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s81" ss:Parent="s16">
   <Alignment ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="10" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#CCCCCC" ss:Pattern="Solid"/>
  </Style>
 </Styles>
 <?php if( $payroll_groups ) {  
$total_net_pay = 0;
$pc_count = (count($print_columns)) ? count($print_columns) : 1;
$column_width = ceil(71 / $pc_count);

$total_earnings_cell = 2;
$columns_count = 1;
if( $column_group_salaries ) { 
  if( isColumn($this, 'working_days', $print_columns) ) { 
    $columns_count += 1;
    $total_earnings_cell += 1;
  } 
  if( isColumn($this, 'absences', $print_columns) ) { 
    $columns_count += 1;
    $total_earnings_cell += 1;
  } 
   if( isColumn($this, 'rate_per_day', $print_columns) ) { 
    $columns_count += 1;
    $total_earnings_cell += 1;
   } 
   if( isColumn($this, 'basic_salary', $print_columns) ) { 
    $columns_count += 1;
    $total_earnings_cell += 1;
   }
   if( isColumn($this, 'cola', $print_columns) ) { 
    $columns_count += 1;
    $total_earnings_cell += 1;
  } 
   if( isColumn($this, 'absences_amount', $print_columns) ) { 
    $columns_count += 1;
    $total_earnings_cell += 1;
   } 
   if( isColumn($this, 'net_pay', $print_columns) ) { 
    $columns_count += 1;
    $total_earnings_cell += 1;
   } 
   if( isColumn($this, 'leave_benefits', $print_columns) ) { 
    $columns_count += 1;
    $total_earnings_cell += 1;
   } 
  if( isColumn($this, 'gross_pay', $print_columns) ) { 
  $columns_count += 1;
  $total_earnings_cell += 1;
  } 
} 

if( $column_group_earnings ) {
if( $earnings_columns ) foreach( $earnings_columns as $column ) {
if( isColumn($this, 'earning_'.$column->id, $print_columns) ) {
    $columns_count += 1;
    $total_earnings_cell += 1;
}
}
}
if( $column_group_salaries || $column_group_earnings) {
    $columns_count += 1;
}
if( $column_group_benefits ) {
if( $benefits_columns ) foreach( $benefits_columns as $column ) {
if( isColumn($this, 'benefit_'.$column->id, $print_columns) ) {
    $columns_count += 2;
}
}
}
if( $column_group_deductions ) {
if( $deductions_columns ) foreach( $deductions_columns as $column ) {
if( isColumn($this, 'deduction_'.$column->id, $print_columns) ) {
    $columns_count += 1;
}
}
}
 if( $column_group_benefits||$column_group_deductions ) {
    $columns_count += 1;
}
  $columns_count += 1;

$rows = 0;
foreach($payroll_groups as $pg) {
  // header
  $rows += 1;
  $rows += count( $pg->employees );
  // group total
  $rows += 1;
  $rows += 1;
}
// grand total
$rows += 1;

$overall_gross_cell = array();
?>
 <Worksheet ss:Name="Sheet1">
  <Names>
   <NamedRange ss:Name="Print_Titles" ss:RefersTo="=Sheet1!C1"/>
  </Names>
  <Table ss:ExpandedColumnCount="<?php echo $columns_count; ?>" ss:ExpandedRowCount="<?php echo $rows; ?>" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
   <Column ss:AutoFitWidth="0" ss:Width="176.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="100" ss:Span="<?php echo ($columns_count-2); ?>"/>
<?php foreach($payroll_groups as $payroll_group) { ?>
  <?php if($payroll_group->employees) { ?>
   <Row ss:AutoFitHeight="0" ss:Height="33.75">
    <Cell ss:StyleID="s62"><Data ss:Type="String"><?php echo strtoupper($payroll_group->name); ?></Data><NamedCell
      ss:Name="Print_Titles"/></Cell>
<?php if( $column_group_salaries ) { ?>
<?php if( isColumn($this, 'working_days', $print_columns) ) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String">WORKING DAYS</Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'absences', $print_columns) ) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String">ABSENCES</Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'rate_per_day', $print_columns) ) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String">RATE PER DAY</Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'basic_salary', $print_columns) ) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String">BASIC SALARY</Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'cola', $print_columns) ) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String">COLA</Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'absences_amount', $print_columns) ) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String">ABSENCES</Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'net_pay', $print_columns) ) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String">NET SALARY</Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'leave_benefits', $print_columns) ) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String">LEAVE BENEFITS</Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'gross_pay', $print_columns) ) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String">GROSS PAY</Data></Cell>
<?php } ?>
<?php } ?>
<?php if( $column_group_earnings ) { ?>
<?php if( $earnings_columns ) foreach( $earnings_columns as $column ) { ?>
<?php if( isColumn($this, 'earning_'.$column->id, $print_columns) ) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String"><?php echo strtoupper($column->name); ?></Data></Cell>
<?php } ?>
<?php } ?>
<?php } ?>
<?php if( $column_group_salaries || $column_group_earnings) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String">TOTAL EARNINGS</Data></Cell>
<?php } ?>
<?php if( $column_group_benefits ) { ?>
<?php if( $benefits_columns ) foreach( $benefits_columns as $column ) { ?>
<?php if( isColumn($this, 'benefit_'.$column->id, $print_columns) ) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String"><?php echo strtoupper($column->name); ?>-EE</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="String"><?php echo strtoupper($column->name); ?>-ER</Data></Cell>
<?php } ?>
<?php } ?>
<?php } ?>
<?php if( $column_group_deductions ) { ?>
<?php if( $deductions_columns ) foreach( $deductions_columns as $column ) { ?>
<?php if( isColumn($this, 'deduction_'.$column->id, $print_columns) ) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String"><?php echo strtoupper($column->name); ?></Data></Cell>
<?php } ?>
<?php } ?>
<?php } ?>
 <?php if( $column_group_benefits || $column_group_deductions ) { ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String">TOTAL DEDUCTIONS</Data></Cell>
<?php } ?>
    <Cell ss:StyleID="s63"><Data ss:Type="String">NET PAY</Data></Cell>
   </Row>
<?php 

$group_basic_salary = 0;
$group_absences = 0;
$group_net_salary = 0;
$group_leave_benefits = 0;
$group_gross_pay = 0;
$group_total_earnings = 0;
$group_total_deductions = 0;
$group_net_pay = 0;
$group_earnings = array();
$group_benefits = array();
$group_deductions = array();

$inner_row = 0;
foreach($payroll_group->employees as $employee) {

$total_deductions = 0;
$total_earnings = 0;
$working_hours = ($employee->working_hours) ? $employee->working_hours : 8;
$days_absent = ($employee->absences_hours) ? ($employee->absences_hours / $working_hours) : 0;
$days_benefits = ($employee->leave_benefits) ? ($employee->leave_benefits / $working_hours) : 0;
$present_days = $inclusive_dates->working_days - $days_absent;
$monthly_rate = 0;
$daily_rate = 0;
$hourly_rate = 0;
$cola_rate = 0;
$absences = 0;
$basic_salary = 0;
$addon_benefits = 0;

if( $employee->salary ) {
  $salary = $employee->salary;
  switch( $salary->rate_per ) {
    case 'month':
      $monthly_rate = $salary->amount;
      $daily_rate = ( ($salary->amount * $salary->months) / $salary->annual_days );
      $hourly_rate = ( (($salary->amount * $salary->months) / $salary->annual_days) / $salary->hours );
    break;
    case 'day':
      $monthly_rate = ( $salary->amount * $salary->days );
      $daily_rate = $salary->amount;
      $hourly_rate = ( $salary->amount / $salary->hours );
    break;
    case 'hour':
      $monthly_rate = ( $salary->amount * $salary->days * $salary->hours );
      $daily_rate = ( $salary->amount * $salary->hours );
      $hourly_rate = $salary->amount;
    break;
  }
  $cola_rate = (isset($salary)) ? $salary->cola : 0;
  $absences = $days_absent * $daily_rate;
  $addon_benefits = $days_benefits * $daily_rate;

  switch( $salary->manner ) {
      case 'hourly':
        $basic_salary = ($hourly_rate * $inclusive_dates->working_days * $salary->hours); 
      break;
      case 'semi-monthly':
        $basic_salary = ($daily_rate * $salary->days) / 2; 
      break;
      case 'monthly':
        $basic_salary = ($daily_rate * $salary->days); 
      break;
      default:
      case 'daily':
        $basic_salary = ($daily_rate * $inclusive_dates->working_days); 
      break;
  }
}

$cola = ($cola_rate * $present_days);
$net_pay = (($basic_salary + $cola) - $absences); 
$gross_pay = $net_pay + $addon_benefits;
$group_basic_salary += $basic_salary;
$group_absences += $absences;
$group_gross_pay += $gross_pay;

$inner_row++;

?>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:StyleID="s64"><Data ss:Type="String"><?php echo $employee->lastname; ?>, <?php echo $employee->firstname; ?> <?php echo ($employee->middlename) ? substr($employee->middlename,0,1)."." : ""; ?></Data><NamedCell
      ss:Name="Print_Titles"/></Cell>
<?php $formula1 = ''; ?>
<?php if( $column_group_salaries ) { 
$working_days_column = 1;
$days_absent_column = 2;
$daily_rate_column = 1;
?>
<?php if( isColumn($this, 'working_days', $print_columns) ) { ?>
    <Cell ss:StyleID="s65"><Data ss:Type="Number"><?php echo $inclusive_dates->working_days; ?></Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'absences', $print_columns) ) { 
$working_days_column++;
?>
    <Cell ss:StyleID="s65"><Data ss:Type="Number"><?php echo $days_absent; ?></Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'rate_per_day', $print_columns) ) {
$working_days_column++;
?>
    <Cell ss:StyleID="s651"><Data ss:Type="Number"><?php echo $daily_rate; ?></Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'basic_salary', $print_columns) ) { 
$days_absent_column++;
$daily_rate_column++;
?>
<?php $formula1 = 'RC[-2]'; ?>
<?php if( $salary->manner == 'daily' ) { ?>
    <Cell ss:StyleID="s651" ss:Formula="=RC[-<?php echo $working_days_column; ?>]*RC[-1]"><Data ss:Type="Number"><?php echo $basic_salary; ?></Data></Cell>
<?php } else { ?>
    <Cell ss:StyleID="s651"><Data ss:Type="Number"><?php echo $basic_salary; ?></Data></Cell>
<?php } ?>
<?php } ?>
<?php if( isColumn($this, 'cola', $print_columns) ) { 
$days_absent_column++;
$daily_rate_column++;
?>
<?php $formula1 = '(RC[-3]+RC[-2])'; ?>
    <Cell ss:StyleID="s651"><Data ss:Type="Number"><?php echo $cola; ?></Data></Cell>
<?php } ?>
<?php 
if( isColumn($this, 'absences_amount', $print_columns) ) { ?>
<?php $formula1 .= '-RC[-1]'; ?>
    <Cell ss:StyleID="s66" ss:Formula="=RC[-<?php echo $days_absent_column; ?>]*RC[-<?php echo $daily_rate_column; ?>]"><Data ss:Type="Number"><?php echo $absences; ?></Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'net_pay', $print_columns) ) { ?>
                <Cell ss:StyleID="s651" ss:Formula="=<?php echo $formula1; ?>"><Data ss:Type="Number"><?php echo $net_pay; ?></Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'leave_benefits', $print_columns) ) { ?>
                <Cell ss:StyleID="s651"><Data ss:Type="Number"><?php echo $addon_benefits; ?></Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'gross_pay', $print_columns) ) { ?>
    <Cell ss:StyleID="s651" ss:Formula="=(RC[-2]+RC[-1])"><Data ss:Type="Number"><?php echo $gross_pay; ?></Data></Cell>
<?php } ?>
<?php } ?>
<?php if( $column_group_earnings ) { ?>
<?php $earnings_cells = 0; ?>
<?php if( $earnings_columns ) foreach( $earnings_columns as $column ) { ?>
      <?php 
      $var = 'earnings_' . $column->id;
      if( !isset($group_earnings[$var]) ) {
          $group_earnings[$var] = 0;
      }
      $group_earnings[$var] += $employee->$var;
      $total_earnings += $employee->$var;
      if( isColumn($this, 'earning_' . $column->id, $print_columns) ) { ?>
    <Cell ss:StyleID="s651"><Data ss:Type="Number"><?php echo $employee->$var; ?></Data></Cell>
      <?php $earnings_cells++; ?>
      <?php } ?>
    <?php } ?>
<?php } ?>
<?php if( $column_group_salaries || $column_group_earnings) { ?>
<?php 
$total_gross_earnings = ($total_earnings + $gross_pay);
$group_total_earnings += $total_gross_earnings; ?>
    <Cell ss:StyleID="s67" ss:Formula="=SUM(RC[-<?php echo ($earnings_cells+1); ?>]:RC[-1])"><Data ss:Type="Number"><?php echo $total_gross_earnings; ?></Data></Cell>
<?php } ?>
<?php if( $column_group_benefits ) { ?>
<?php $benefits_cells = 0; ?>
<?php if( $benefits_columns ) foreach( $benefits_columns as $column ) { 
  $ee = 'ee_share_' . $column->id;
  if( !isset($group_benefits[$ee]) ) {
          $group_benefits[$ee] = 0;
  }
  $group_benefits[$ee] += $employee->$ee;

  $er = 'er_share_' . $column->id;
  if( !isset($group_benefits[$er]) ) {
          $group_benefits[$er] = 0;
  }

  $group_benefits[$er] += $employee->$er;
  $total_deductions += $employee->$ee;
  if( isColumn($this, 'benefit_' . $column->id, $print_columns) ) {
?>
    <Cell ss:StyleID="s651"><Data ss:Type="Number"><?php echo $employee->$ee; ?></Data></Cell>
    <Cell ss:StyleID="s651"><Data ss:Type="Number"><?php echo $employee->$er; ?></Data></Cell>
<?php $benefits_cells++; ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php if( $column_group_deductions ) { ?>
<?php $deductions_cells = 0; ?>
<?php if( $deductions_columns ) foreach( $deductions_columns as $column ) { 
  $var = 'deductions_' . $column->id;
  if( !isset($group_deductions[$var]) ) {
      $group_deductions[$var] = 0;
   }
   $group_deductions[$var] += $employee->$var;
  $total_deductions += $employee->$var;
  if( isColumn($this, 'deduction_' . $column->id, $print_columns) ) {
  ?>
    <Cell ss:StyleID="s651"><Data ss:Type="Number"><?php echo $employee->$var; ?></Data></Cell>
                <?php $deductions_cells++; ?>
                <?php } ?>
 <?php } ?>
 <?php } ?>
 <?php 
$net_pay = (($total_earnings + $gross_pay) - $total_deductions); 
$total_net_pay += $net_pay;
$group_net_pay += $net_pay;
$group_total_deductions += $total_deductions;
?>
 <?php if( $column_group_benefits||$column_group_deductions ) { ?>
 <?php 
$benefits_formula = '';
if($benefits_cells > 0) { 
  $benefits_formula1 = array();
  for($i=0;$i<$benefits_cells;$i++) {
    $benefits_formula1[] = 'RC[-'.($deductions_cells+(($i+1)*2)).']';
  }
  $benefits_formula = "+" . implode("+", $benefits_formula1);
}
  ?>
    <Cell ss:StyleID="s651" ss:Formula="=SUM(RC[-<?php echo $deductions_cells; ?>]:RC[-1])<?php echo $benefits_formula; ?>"><Data ss:Type="Number"><?php echo $total_deductions; ?></Data></Cell>
<?php } ?>
    <Cell ss:StyleID="s69" ss:Formula="=RC[-<?php echo ($columns_count-$total_earnings_cell); ?>]-RC[-1]"><Data ss:Type="Number"><?php echo $net_pay; ?></Data></Cell>
   </Row>
<?php } ?>
<?php $overall_gross_cell[] = $inner_row; ?>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:StyleID="s70"><Data ss:Type="String">GROUP TOTAL</Data></Cell>
<?php if( $column_group_salaries ) { ?>
<?php if( isColumn($this, 'working_days', $print_columns) ) { ?>
    <Cell ss:StyleID="s71"/>
<?php } ?>
<?php if( isColumn($this, 'absences', $print_columns) ) { ?>
    <Cell ss:StyleID="s71"/>
<?php } ?>
<?php if( isColumn($this, 'rate_per_day', $print_columns) ) { ?>
    <Cell ss:StyleID="s71"/>
<?php } ?>
<?php if( isColumn($this, 'basic_salary', $print_columns) ) { ?>
    <Cell ss:StyleID="s711" ss:Formula="=SUM(R[-<?php echo $inner_row; ?>]C:R[-1]C)"><Data ss:Type="Number">0</Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'cola', $print_columns) ) { ?>
    <Cell ss:StyleID="s711" ss:Formula="=SUM(R[-<?php echo $inner_row; ?>]C:R[-1]C)"><Data ss:Type="Number">0</Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'absences_amount', $print_columns) ) { ?>
    <Cell ss:StyleID="s711" ss:Formula="=SUM(R[-<?php echo $inner_row; ?>]C:R[-1]C)"><Data ss:Type="Number">0</Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'net_pay', $print_columns) ) { ?>
    <Cell ss:StyleID="s711" ss:Formula="=SUM(R[-<?php echo $inner_row; ?>]C:R[-1]C)"><Data ss:Type="Number">0</Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'leave_benefits', $print_columns) ) { ?>
    <Cell ss:StyleID="s711" ss:Formula="=SUM(R[-<?php echo $inner_row; ?>]C:R[-1]C)"><Data ss:Type="Number">0</Data></Cell>
<?php } ?>
<?php if( isColumn($this, 'gross_pay', $print_columns) ) { ?>
    <Cell ss:StyleID="s72" ss:Formula="=SUM(R[-<?php echo $inner_row; ?>]C:R[-1]C)"><Data ss:Type="Number"><?php echo $group_gross_pay; ?></Data></Cell>
<?php } ?>
<?php } ?>
<?php if( $column_group_earnings ) { ?>
<?php if( $earnings_columns ) foreach( $earnings_columns as $column ) { ?>
<?php if( isColumn($this, 'earning_' . $column->id, $print_columns) ) {
  $var = 'earnings_' . $column->id;
?>
    <Cell ss:StyleID="s711" ss:Formula="=SUM(R[-<?php echo $inner_row; ?>]C:R[-1]C)"><Data ss:Type="Number"><?php echo $group_earnings[$var]; ?></Data></Cell>
  <?php } ?>
<?php } ?>
<?php } ?>
<?php if( $column_group_salaries || $column_group_earnings) { ?>
    <Cell ss:StyleID="s72" ss:Formula="=SUM(R[-<?php echo $inner_row; ?>]C:R[-1]C)"><Data ss:Type="Number"><?php echo $group_total_earnings; ?></Data></Cell>
<?php } ?>
<?php if( $column_group_benefits ) { ?>
<?php if( $benefits_columns ) foreach( $benefits_columns as $column ) { ?>
  <?php  if( isColumn($this, 'benefit_' . $column->id, $print_columns) ) { 
$ee = 'ee_share_' . $column->id;
$er = 'er_share_' . $column->id;
    ?>
    <Cell ss:StyleID="s711" ss:Formula="=SUM(R[-<?php echo $inner_row; ?>]C:R[-1]C)"><Data ss:Type="Number"><?php echo $group_benefits[$ee]; ?></Data></Cell>
    <Cell ss:StyleID="s711" ss:Formula="=SUM(R[-<?php echo $inner_row; ?>]C:R[-1]C)"><Data ss:Type="Number"><?php echo $group_benefits[$er]; ?></Data></Cell>
  <?php } ?>
<?php } ?>
<?php } ?>
<?php if( $column_group_deductions ) { ?>
<?php if( $deductions_columns ) foreach( $deductions_columns as $column ) { 
  if( isColumn($this, 'deduction_' . $column->id, $print_columns) ) {
    $var = 'deductions_' . $column->id;
  ?>
    <Cell ss:StyleID="s711" ss:Formula="=SUM(R[-<?php echo $inner_row; ?>]C:R[-1]C)"><Data ss:Type="Number"><?php echo $group_deductions[$var]; ?></Data></Cell>
                 <?php } ?>
 <?php } ?>
 <?php } ?>
 <?php if( $column_group_benefits||$column_group_deductions ) { ?>
    <Cell ss:StyleID="s73" ss:Formula="=SUM(R[-<?php echo $inner_row; ?>]C:R[-1]C)"><Data ss:Type="Number"><?php echo $group_total_deductions; ?></Data></Cell>
<?php } ?>
    <Cell ss:StyleID="s73" ss:Formula="=SUM(R[-<?php echo $inner_row; ?>]C:R[-1]C)"><Data ss:Type="Number"><?php echo $group_net_pay; ?></Data></Cell>
   </Row>
    <Row ss:AutoFitHeight="0" ss:Height="10"/>
<?php } ?>
<?php } ?>  

<?php 
$formula = array();
$cell = $rows+1;
foreach($overall_gross_cell as $onc) {
  $cell = $cell-($onc+3);
  $formula[] = "R[-".$cell."]C";
}
?>

   <Row ss:AutoFitHeight="0" ss:Height="22.5">
    <Cell ss:StyleID="s80"><Data ss:Type="String">TOTAL NET PAY</Data><NamedCell
      ss:Name="Print_Titles"/></Cell>
<?php for( $i=0; $i<($columns_count-2);$i++ ) { ?>
    <Cell ss:StyleID="s81" ss:Formula="=<?php echo implode("+", $formula); ?>"><Data ss:Type="Number">0</Data></Cell>
<?php } ?>
    <Cell ss:StyleID="m79168628" ss:Formula="=<?php echo implode("+", $formula); ?>"><Data ss:Type="Number"><?php echo $total_net_pay; ?></Data></Cell>
   </Row>

  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Layout x:Orientation="Landscape"/>
    <Header x:Margin="0.3"
     x:Data="&amp;L&amp;&quot;Calibri,Bold&quot;&amp;18<?php echo ($company->name) ? strtoupper($company->name) : ''; ?>&amp;&quot;Calibri,Regular&quot;&amp;11&#10;<?php echo ($company->address) ? $company->address : ''; ?>&#10;<?php echo ($company->phone) ? $company->phone : ''; ?>&amp;R&amp;&quot;Calibri,Bold&quot;&amp;16PAYROLL SUMMARY (#<?php echo $payroll->id; ?>)&amp;&quot;Calibri,Regular&quot;&amp;11&#10;<?php echo $payroll->name; ?>"/>
    <Footer x:Margin="0.3"
     x:Data="&amp;L&amp;&quot;Calibri,Bold&quot;Prepared by:&amp;&quot;Calibri,Regular&quot;&#10;&#10;<?php echo strtoupper($this->session->name); ?>&amp;C&amp;&quot;Calibri,Bold&quot;Checked by:&amp;&quot;Calibri,Regular&quot;&#10;&#10;<?php echo strtoupper($template->checked_by_name); ?>&amp;R&amp;&quot;Calibri,Bold&quot;Approved by:&amp;&quot;Calibri,Regular&quot;&#10;&#10;<?php echo strtoupper($template->approved_by_name); ?>"/>
    <PageMargins x:Bottom="1" x:Left="0.7" x:Right="0.7" x:Top="1.1"/>
   </PageSetup>
   <Unsynced/>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>200</PaperSizeIndex>
    <Scale>78</Scale>
    <HorizontalResolution>-3</HorizontalResolution>
    <VerticalResolution>-3</VerticalResolution>
   </Print>
   <ShowPageBreakZoom/>
   <PageBreakZoom>100</PageBreakZoom>
   <Selected/>
   <LeftColumnVisible>4</LeftColumnVisible>
   <Panes>
    <Pane>
     <Number>3</Number>
     <RangeSelection>R1:R6</RangeSelection>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
 <?php } ?>
</Workbook>